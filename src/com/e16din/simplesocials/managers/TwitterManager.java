package com.e16din.simplesocials.managers;

import java.io.ByteArrayInputStream;

import twitter4j.StatusUpdate;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationBuilder;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;

import com.e16din.lightutils.Utils;
import com.e16din.simplesocials.core.BaseSocialActionBarActivity;
import com.e16din.simplesocials.core.interfaces.BaseSocialManger;
import com.e16din.simplesocials.core.interfaces.SuccessListener;
import com.e16din.simplesocials.core.utils.ImageLoader;
import com.e16din.simplesocials.core.utils.ImageLoader.OnImageLoaderListener;
import com.e16din.simplesocials.screens.TwitterLoginActivity;

public class TwitterManager extends BaseSocialManger {
	public final static String CALLBACK_URL = "http://autochmo.ru/";

	public static final String PREF_KEY_TWITTER_OAUTH_SECRET = "secret";

	private static final int LENGTH_LIMIT = 91;

	protected String tokenSecret = null;

	private static SuccessListener authListener = null;

	private String consumerKey = null;// XXX: set before posting
	private String consumerSecret = null;// XXX: set before posting

	public TwitterManager(BaseSocialActionBarActivity activity) {
		super(activity);
		tokenSecret = sharedPreferences.getString(PREF_KEY_TWITTER_OAUTH_SECRET, "");
	}

	@Override
	public String getName() {
		return "Twitter";
	}

	public void openTwitterSession() {
		Intent intent = new Intent();
		intent.setClass((Activity) getContextActivity(), TwitterLoginActivity.class);
		((Activity) getContextActivity()).startActivityForResult(intent, 0);
	}

	@Override
	public void login(SuccessListener listener) {
		super.login(listener);

		authListener = listener;

		if (Utils.isOnline((Activity) getContextActivity())) {
			if (!isAuth()) {
				openTwitterSession();
				return;
			}

			setToken(sharedPreferences.getString(PREF_KEY_OAUTH_TOKEN + getName(), ""));
			authListener.onSuccess(this, null);
		} else
			onNoInternetConnection(listener);
	}

	@Override
	public void logout(SuccessListener listener) {
		super.logout(listener);

		setToken(sharedPreferences.getString(PREF_KEY_OAUTH_TOKEN + getName(), ""));
		listener.onSuccess(this, null);
	}

	@Override
	public void onActivityResult(Activity activity, int requestCode, int resultCode, Intent data) {
		if (resultCode == Activity.RESULT_CANCELED)
			authListener.onCancel();

		if (resultCode == Activity.RESULT_OK || isAuth()) {
			setToken(sharedPreferences.getString(PREF_KEY_OAUTH_TOKEN + getName(), ""));
			authListener.onSuccess(this, null);
		}
	}

	private String calculateText(String description, String postURL) {
		if (description == null) {
			description = "";
		}
		if (description.length() == 0) {
			return postURL;
		} else if (description.length() <= LENGTH_LIMIT) {
			return '"' + description + "\" " + postURL;
		} else {
			return '"' + description.substring(0, LENGTH_LIMIT - 3) + "...\" " + postURL;
		}
	}

	@Override
	protected void postPhoto(final String message, final String postUrl, final String mediaUrl,
			final SuccessListener listener) {

		if (!isAuth()) {
			loginBeforeSharing(false, message, postUrl, mediaUrl, listener);
			return;
		}

		new ImageLoader(mediaUrl, new OnImageLoaderListener() {
			@Override
			public void onCompleted(byte[] data, Bitmap bitmap) {
				StatusUpdate status = new StatusUpdate(calculateText(message, postUrl));
				status.setMedia(mediaUrl.substring(mediaUrl.lastIndexOf("/") + 1), new ByteArrayInputStream(data));
				new TweetSender(postUrl, status, listener).execute();
			}
		}).execute();
	}

	@Override
	protected void postVideo(String message, String postUrl, String mediaUrl, SuccessListener listener) {
		if (!isAuth()) {
			loginBeforeSharing(false, message, postUrl, mediaUrl, listener);
			return;
		}

		StatusUpdate status = new StatusUpdate(calculateText(message, postUrl + " http://www.youtube.com/watch?v="
				+ mediaUrl));
		new TweetSender(postUrl, status, listener).execute();
	}

	public String getConsumerKey() {
		return consumerKey;
	}

	public void setConsumerKey(String consumerKey) {
		this.consumerKey = consumerKey;
	}

	public String getConsumerSecret() {
		return consumerSecret;
	}

	public void setConsumerSecret(String consumerSecret) {
		this.consumerSecret = consumerSecret;
	}

	public class TweetSender extends AsyncTask<Void, Void, Integer> {
		private static final int RESULT_FAIL = 1;
		private static final int RESULT_SUCCESS = 0;
		private String url = null;
		private StatusUpdate status = null;
		private SuccessListener listener = null;

		public TweetSender(String url, StatusUpdate status, SuccessListener listener) {
			this.url = url;
			this.status = status;
			this.listener = listener;
		}

		@Override
		protected Integer doInBackground(Void... params) {
			String token = sharedPreferences.getString(PREF_KEY_OAUTH_TOKEN + getName(), "");
			String secret = sharedPreferences.getString(PREF_KEY_TWITTER_OAUTH_SECRET, "");

			ConfigurationBuilder builder = new ConfigurationBuilder();
			builder.setOAuthConsumerKey(getConsumerKey());
			builder.setOAuthConsumerSecret(getConsumerSecret());
			builder.setOAuthAccessToken(token);
			builder.setOAuthAccessTokenSecret(secret);

			Configuration conf = builder.build();

			Twitter twitter = new TwitterFactory(conf).getInstance();

			try {
				twitter.updateStatus(status);
				return RESULT_SUCCESS;
			} catch (TwitterException e) {
				e.printStackTrace();
				return RESULT_FAIL;
			}
		}

		@Override
		protected void onPostExecute(Integer result) {
			if (result.intValue() == RESULT_SUCCESS) {
				if (listener != null)
					listener.onSuccess(TwitterManager.this, url.substring(url.lastIndexOf("/") + 1));
			} else {
				if (listener != null)
					listener.onFail("twitter error", TwitterManager.this);
			}
		}
	}
}
