package com.e16din.simplesocials.screens;

import org.scribe.builder.ServiceBuilder;
import org.scribe.builder.api.TwitterApi;
import org.scribe.model.OAuthRequest;
import org.scribe.model.Token;
import org.scribe.model.Verb;
import org.scribe.model.Verifier;
import org.scribe.oauth.OAuthService;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.e16din.simplesocials.managers.TwitterManager;
import com.e16din.simplesocials.managers.twitter.R;

public class TwitterLoginActivity extends Activity {

	private class RequestSender extends AsyncTask<String, Void, Token> {
		private static final String VERIFY_CREDENTIALS = "https://api.twitter.com/1.1/account/verify_credentials.json";
		private static final String KEY_OAUTH_VERIFIER = "oauth_verifier";
		
		@Override
		protected Token doInBackground(String... params) {
			Uri uri = Uri.parse(params[0]);
			String verifier = uri.getQueryParameter(KEY_OAUTH_VERIFIER);

			Verifier v = new Verifier(verifier);

			Token accessToken = oauthService.getAccessToken(requestToken, v);
			OAuthRequest req = new OAuthRequest(Verb.GET, VERIFY_CREDENTIALS);
			oauthService.signRequest(accessToken, req);

			return accessToken;
		}

		@Override
		protected void onPostExecute(Token accessToken) {
			SharedPreferences pref = getSharedPreferences(TwitterManager.PREFERENCE_NAME, Context.MODE_PRIVATE);
			Editor editor = pref.edit();

			editor.putString(TwitterManager.PREF_KEY_OAUTH_TOKEN+"Twitter", accessToken.getToken());
			editor.putString(TwitterManager.PREF_KEY_TWITTER_OAUTH_SECRET, accessToken.getSecret());

			editor.commit();

			Intent data=new Intent();
			data.putExtra("token", accessToken.getToken());
			setResult(RESULT_OK, data);
			finish();
		}
	}

	private class TwitterWebViewClient extends WebViewClient {
		private static final String CALLBACK_DENIED = TwitterManager.CALLBACK_URL + "?denied";
		
		@Override
		public void onPageFinished(WebView view, String url) {
		}

		@Override
		public void onPageStarted(WebView webview, String url, Bitmap favicon) {
			if (url.startsWith(CALLBACK_DENIED)) {
				setResult(RESULT_CANCELED);
				finish();
				return;
			}

			if (!url.startsWith(TwitterManager.CALLBACK_URL)) {
				super.onPageStarted(webview, url, favicon);
				return;
			}
			webview.setVisibility(View.INVISIBLE);
			new RequestSender().execute(url);
		}
	}
	
	private class WebLoader extends AsyncTask<Void, Void, String> {
		@Override
		protected String doInBackground(Void... params) {
			requestToken = oauthService.getRequestToken();
			return oauthService.getAuthorizationUrl(requestToken);
		}

		@Override
		protected void onPostExecute(String authUrl) {
			webview.loadUrl(authUrl);
		}
	}

	private OAuthService oauthService = null;
	private Token requestToken = null;
	private WebView webview = null;

	@SuppressLint("SetJavaScriptEnabled")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_twitter_login);

		webview = (WebView) findViewById(R.id.wv_twitter_login);

		webview.getSettings().setJavaScriptEnabled(true);
		webview.clearCache(true);
		webview.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
		webview.setWebViewClient(new TwitterWebViewClient());

		String consumerKey = ((TwitterManager)TwitterManager.getCurrentManager()).getConsumerKey();
		String consumerSecret = ((TwitterManager)TwitterManager.getCurrentManager()).getConsumerSecret();
		oauthService = new ServiceBuilder().provider(TwitterApi.class).apiKey(consumerKey).apiSecret(consumerSecret)
				.callback(TwitterManager.CALLBACK_URL).build();

		new WebLoader().execute();
	}
}
